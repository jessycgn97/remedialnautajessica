package ec.edu.ups.appdis.RemedialNautaJessica;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import ec.edu.ups.controlador.DAOCandidato;
import ec.edu.ups.controlador.DAOVotante;
import ec.edu.ups.modelo.Candidato;
import ec.edu.ups.modelo.Eleccion;
import ec.edu.ups.modelo.Votante;
import ec.edu.ups.negocio.EleccionONLocal;


@ManagedBean
@ViewScoped
public class EleccionBeans {
	
	@Inject
	private EleccionONLocal on;
	
	private Candidato candidato;
	
	private Eleccion eleccion;
	
	private Votante votante;
	
	private DAOVotante daovotante;
	
	private DAOCandidato daocandidato;
	
	private List<Eleccion> addEleccion;
	
	private List<Candidato> listCandidato;
	
	@PostConstruct
	public void init() {
		candidato = new Candidato();
		eleccion = new Eleccion();
		votante = new Votante();
		listCandidato = new ArrayList<Candidato>();
		addEleccion = new ArrayList<Eleccion>();
		loadDataCandidato();
	}

	public Candidato getCandidato() {
		return candidato;
	}

	public void setCandidato(Candidato candidato) {
		this.candidato = candidato;
	}

	public List<Eleccion> getAddEleccion() {
		return addEleccion;
	}

	public void setAddEleccion(List<Eleccion> addEleccion) {
		this.addEleccion = addEleccion;
	}
	
	public Eleccion getEleccion() {
		return eleccion;
	}

	public void setEleccion(Eleccion eleccion) {
		this.eleccion = eleccion;
	}

	public Votante getVotante() {
		return votante;
	}

	public void setVotante(Votante votante) {
		this.votante = votante;
	}

	public List<Candidato> getListCandidato() {
		return listCandidato;
	}

	public void setListCandidato(List<Candidato> listCandidato) {
		this.listCandidato = listCandidato;
	}

	
	public DAOVotante getDaovotante() {
		return daovotante;
	}

	public void setDaovotante(DAOVotante daovotante) {
		this.daovotante = daovotante;
	}

	public DAOCandidato getDaocandidato() {
		return daocandidato;
	}

	public void setDaocandidato(DAOCandidato daocandidato) {
		this.daocandidato = daocandidato;
	}

	public void guardarELeccion() {
		on.guardarEleccion(eleccion);
		on.guardarCandidato(candidato);
	}
	
	/*
	 * public void guardarELeccion() {
		on.guardarEleccion(eleccion);
		for(Candidato candi : listCandidato) {
			Candidato candidat = new Candidato();
			candidat.setEleccion(eleccion);
			candidat.setCedula(candi.getCedula());
			candidat.setNombre(candi.getNombre());
			candidat.setNumLista(candi.getNumLista());
			
			on.guardarCandidato(candidato);
		}
		
	}
	 */
	
	public void guardarVotante(Votante votante, Candidato candidato) throws Exception{
		Votante vot = daovotante.read(votante.getCedula());
		if(vot != null) {
			new Throwable("Ud ya dio su voto a un candidato");
		}else {
			daovotante.insertar(votante);
			int votos = candidato.getNumVotos();
			candidato.setNumVotos(votos+1);
			daocandidato.update(candidato);
		}
		//on.guardarVotante(votante);
	}
	
	public void addCandidato() {
		listCandidato.add(new Candidato());
		//eleccion.agregarCandidato(candidato);
	}
	
	private void loadDataCandidato() {
		listCandidato = on.getCandidato();

	}
	
	public void votar() {
		int voto = 0;
		int votoLista1=0;
		int votoLista2=0;
		int opcion = 0;
		
	}

}
