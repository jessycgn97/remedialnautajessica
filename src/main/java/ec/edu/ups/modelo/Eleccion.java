package ec.edu.ups.modelo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Eleccion implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int elec_id;
	
	private String titulo;
	private Date fechaVigencia;
	
	/*@ManyToOne
	@JoinColumn(name="can_id")
    private Candidato candidato;

	public void agregarCandidato(Candidato can) {
		if(candidato==null) {
			candidato =  new Candidato();
		Eleccion elec = new Eleccion();
		elec.setCandidato(can);
		}
	}*/
	
	public Eleccion() {
	}

	public int getElec_id() {
		return elec_id;
	}

	public void setElec_id(int elec_id) {
		this.elec_id = elec_id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Date getFechaVigencia() {
		return fechaVigencia;
	}

	public void setFechaVigencia(Date fechaVigencia) {
		this.fechaVigencia = fechaVigencia;
	}

	/*public Candidato getCandidato() {
		return candidato;
	}

	public void setCandidato(Candidato candidato) {
		this.candidato = candidato;
	}*/
	
}
