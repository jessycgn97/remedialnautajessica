package ec.edu.ups.controlador;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import ec.edu.ups.modelo.Candidato;
import ec.edu.ups.modelo.Eleccion;


@Stateless
public class DAOEleccion {
	
	@PersistenceContext(name="RemedialNautaJessicaPersistenceUnit")
	private EntityManager em;
	
	public void insertar(Eleccion eleccion) {
		em.persist(eleccion);
	}
	
	public List<Eleccion> getElecciones() {
		String jpql = "SELECT e FROM Eleccion e ";
		Query q = em.createQuery(jpql, Eleccion.class);		
		return q.getResultList();
	}

}
