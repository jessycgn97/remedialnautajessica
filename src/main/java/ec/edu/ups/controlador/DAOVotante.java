package ec.edu.ups.controlador;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import ec.edu.ups.modelo.Candidato;
import ec.edu.ups.modelo.Votante;


@Stateless
public class DAOVotante {
	
	@PersistenceContext(name="RemedialNautaJessicaPersistenceUnit")
	private EntityManager em;
	
	public void insertar(Votante votante) {
		em.persist(votante);
	}
	
	public Votante read(String cedula) {
		return em.find(Votante.class, cedula);
	}
	
	public List<Votante> getVotantes() {
		String jpql = "SELECT v FROM Votante v ";
		Query q = em.createQuery(jpql, Votante.class);		
		return q.getResultList();
	}

}
