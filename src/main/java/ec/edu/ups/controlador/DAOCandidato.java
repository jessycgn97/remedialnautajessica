package ec.edu.ups.controlador;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import ec.edu.ups.modelo.Candidato;


@Stateless
public class DAOCandidato {
	
	@PersistenceContext(name="RemedialNautaJessicaPersistenceUnit")
	private EntityManager em;
	
	public void insertar(Candidato candidato) {
		em.persist(candidato);
	}
	
	public void update(Candidato candidato) {
		em.merge(candidato);
	}
	
	public List<Candidato> getCandidatos() {
		String jpql = "SELECT c FROM Candidato c ";
		Query q = em.createQuery(jpql, Candidato.class);		
		return q.getResultList();
	}
	
	public List<Candidato> listarCandidatos(int numLista) {
		String jpql = "SELECT c FROM Candidato c WHERE numLista = :numLista";
		Query q = em.createQuery(jpql, Candidato.class);		
		q.setParameter("numLista", numLista);
		return q.getResultList();
	}

}
