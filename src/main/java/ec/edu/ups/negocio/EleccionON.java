package ec.edu.ups.negocio;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import ec.edu.ups.controlador.DAOCandidato;
import ec.edu.ups.controlador.DAOEleccion;
import ec.edu.ups.controlador.DAOVotante;
import ec.edu.ups.modelo.Candidato;
import ec.edu.ups.modelo.Eleccion;
import ec.edu.ups.modelo.Votante;

@Stateless
public class EleccionON implements EleccionONRemoto, EleccionONLocal{

	@Inject 
	private DAOCandidato daocandidato;
	
	@Inject
	private DAOEleccion daoeleccion;
	
	@Inject
	private DAOVotante daovotante;
	
	public void guardarCandidato(Candidato candidato) {
		daocandidato.insertar(candidato);
	}
	
	public void guardarEleccion(Eleccion eleccion) {
		daoeleccion.insertar(eleccion);
	}
	
	public void guardarVotante(Votante votante) {
		daovotante.insertar(votante);
	}
	
	public List<Candidato> getCandidato(){
		return daocandidato.getCandidatos();
	}
    
    public List<Eleccion> getEleccion(){
    	return daoeleccion.getElecciones();
    }
    
    public List<Votante> getVotante(){
    	return daovotante.getVotantes();
    }
}
