package ec.edu.ups.negocio;

import java.util.List;

import javax.ejb.Remote;

import ec.edu.ups.modelo.Candidato;
import ec.edu.ups.modelo.Eleccion;
import ec.edu.ups.modelo.Votante;

@Remote
public interface EleccionONRemoto {
	
	public void guardarCandidato(Candidato candidato);
	
	public void guardarEleccion(Eleccion eleccion);
	
	public void guardarVotante(Votante votante);
	
	public List<Candidato> getCandidato();
    
    public List<Eleccion> getEleccion();
    
    public List<Votante> getVotante();

}
