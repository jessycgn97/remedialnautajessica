package ec.edu.ups.servicios;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;

import ec.edu.ups.modelo.Candidato;
import ec.edu.ups.modelo.Eleccion;
import ec.edu.ups.negocio.EleccionONLocal;

@WebService
public class Soap {

	@Inject
	private EleccionONLocal on;
	
	@WebMethod
	public void guardarDatos(String cedula, String nombre, int numLista, String titulo) {
		Candidato cand = new Candidato();
		cand.setCedula(cedula);
		cand.setNombre(nombre);
		cand.setNumLista(numLista);
		on.guardarCandidato(cand);
		Eleccion elec = new Eleccion();
		elec.setTitulo(titulo);
		on.guardarEleccion(elec);
	}
	
	@WebMethod
	public List<Candidato> listaCandidatoo(){
		List<Candidato> listado = new ArrayList<Candidato>();
		try {
			listado = on.getCandidato();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return listado;
	}
}
